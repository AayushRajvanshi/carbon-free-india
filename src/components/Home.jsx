import React, { Component } from 'react';
import Slider1 from '../images/slider-1.jpg';
import footerLogo from '../images/footer_logo.png';
import tree from '../images/tree.png';
import man from '../images/man.png';

class Home extends Component {

    render() {
        return (
            <div>
                <div id="slider" className="uk-section uk-section-default uk-padding-remove">
                    <div className="uk-inline">
                        <div className="uk-background-cover uk-panel uk-flex uk-flex-center uk-flex-middle" style={{ 'height': '70vh', 'width': '98.6vw', 'background-image': 'url(' + Slider1 + ')' }}></div>
                        <div className="uk-overlay uk-light uk-position-center-left">
                            <div className="uk-h1 uk-text-bold uk-margin-remove uk-margin-large-left uk-text-uppercase" style={{ 'color': 'yellowgreen', '-webkit-text-stroke': '1px yellow' }}></div>
                            <div className="uk-h3 uk-margin-remove uk-margin-large-left" style={{ 'color': 'yellowgreen' }}></div>
                        </div>
                    </div>
                </div>

                <div id="section-1" className="uk-section uk-section-muted uk-padding-remove">
                    <div className="uk-grid-collapse uk-child-width-1-2@s uk-child-width-1-4@m uk-text-center" data-uk-grid>
                        <div className="uk-background-muted uk-height-large uk-padding" style={{ 'background-image': 'url(//cdn.shopify.com/s/files/1/1828/4857/files/gallary1_512x531_crop_center.jpg)' }}>
                            <div className="uk-padding">
                                <div style={{ 'color': 'white' }}>
                                    <span data-uk-icon="icon: album; ratio: 5"></span>
                                </div>
                                <div className="uk-h3 uk-text-uppercase uk-margin-small" style={{ 'color': 'white' }}>Calculate</div>
                                <div className="uk-h3 uk-text-uppercase uk-margin-small" style={{ 'color': 'white' }}>Your Carbon Emission</div>
                            </div>
                        </div>
                        <div className="uk-background-muted uk-height-large uk-padding" style={{ 'background-image': 'url(//cdn.shopify.com/s/files/1/1828/4857/files/gallary2_512x531_crop_center.jpg)' }}>
                            <div className="uk-padding">
                                <div style={{ 'color': 'white' }}>
                                    <span data-uk-icon="icon: thumbnails; ratio: 5"></span>
                                </div>
                                <div className="uk-h3 uk-text-uppercase uk-margin-small" style={{ 'color': 'white' }}>Carban Offset</div>
                                <div className="uk-h3 uk-text-uppercase uk-margin-small" style={{ 'color': 'white' }}>How it Works?</div>
                            </div>
                        </div>
                        <div className="uk-background-muted uk-height-large uk-padding" style={{ 'background-image': 'url(//cdn.shopify.com/s/files/1/1828/4857/files/gallary3_512x531_crop_center.jpg)' }}>
                            <div className="uk-padding">
                                <div style={{ 'color': 'white' }}>
                                    <span data-uk-icon="icon: paint-bucket; ratio: 5"></span>
                                </div>
                                <div className="uk-h3 uk-text-uppercase uk-margin-small" style={{ 'color': 'white' }}>Our Policies</div>
                                <div className="uk-h3 uk-text-uppercase uk-margin-small" style={{ 'color': 'white' }}></div>
                            </div>
                        </div>
                        <div className="uk-background-muted uk-height-large uk-padding" style={{ 'background-image': 'url(//cdn.shopify.com/s/files/1/1828/4857/files/gallary4_512x531_crop_center.jpg)' }}>
                            <div className="uk-padding">
                                <div style={{ 'color': 'white' }}>
                                    <span data-uk-icon="icon: list; ratio: 5"></span>
                                </div>
                                <div className="uk-h3 uk-text-uppercase uk-margin-small" style={{ 'color': 'white' }}>Additional</div>
                                <div className="uk-h3 uk-text-uppercase uk-margin-small" style={{ 'color': 'white' }}></div>
                            </div>
                        </div>
                    </div>
                </div>

                <div id="section-2" className="uk-section uk-section-muted">
                    <div className="uk-container">
                        <div className="uk-child-width-1-2@m uk-grid uk-grid-match" data-uk-grid>
                            <div className="uk-flex uk-flex-middle">
                                <div className="uk-tile uk-tile-default">
                                    <div className="uk-background-cover" style={{ 'background-image': 'url(' + tree + ')', 'height': '25em' }}></div>
                                </div>
                            </div>
                            <div className="uk-flex uk-flex-middle">
                                <div className="uk-tile uk-tile-default">
                                    <div className="uk-h1 uk-text-bold uk-margin-remove uk-text-uppercase uk-margin-small-bottom">Cillum ipsum sit velit sunt cillum officia</div>
                                    <div className="uk-h4 uk-margin-small" style={{ 'color': '#808080' }}>Deserunt sit do enim et reprehenderit enim anim ea enim id minim nostrud velit</div>
                                    <button className="uk-button uk-button-default uk-button-large uk-width-1-2" style={{ 'border-radius': '50px' }}>More Info...</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div id="section-3" className="uk-section uk-section-muted uk-preserve-color">
                    <div className="uk-container">
                        <div className="uk-child-width-1-2@m uk-grid uk-grid-match" data-uk-grid>
                            <div className="uk-flex uk-flex-middle uk-padding-large">
                                <div className="uk-h1 uk-text-bold uk-margin-remove uk-text-uppercase uk-margin-small-bottom" style={{ 'color': 'white' }}>Pariatur officia quis irure ex</div>
                                <div className="uk-h4 uk-margin-small" style={{ 'color': 'white' }}>Sunt esse eu veniam reprehenderit fugiat ipsum commodo. Voluptate Lorem deserunt commodo aute ullamco commodo nostrud sint nisi qui mollit.</div>
                                <button className="uk-button uk-button-default uk-button-large uk-width-1-2" style={{ 'color': 'white', 'border-radius': '50px' }}>More Info...</button>
                            </div>
                        </div>
                    </div>
                </div>

                <div id="section-4" className="uk-section uk-section-muted uk-padding-remove">
                    <div className="uk-container">
                        <div className="uk-child-width-1-2@m uk-grid uk-grid-match" data-uk-grid>
                            <div className="uk-flex uk-flex-middle">
                                <div className="uk-background-cover" style={{ 'background-image': 'url(' + man + ')', 'height': '35em' }}></div>
                            </div>
                            <div className="uk-flex uk-flex-middle">
                                <div className="uk-tile uk-tile-default">
                                    <div className="uk-h1 uk-text-bold uk-margin-remove uk-text-uppercase uk-margin-small-bottom">Proident dolor in Lorem officia</div>
                                    <div className="uk-h4 uk-margin-small" style={{ 'color': '#808080' }}>Pariatur voluptate exercitation laborum est ad culpa excepteur do laboris proident ad do id.</div>
                                    <div className="uk-h5 uk-margin-small" style={{ 'color': '#808080' }}>Fugiat dolore irure aliqua culpa laboris aliqua adipisicing aliquip deserunt cillum sit. Lorem cupidatat minim ad nisi sit qui sit ad dolore qui mollit mollit occaecat tempor. Dolor minim consequat excepteur non dolore dolor laborum consectetur in.</div>
                                    <div className="uk-h3 uk-margin-small" style={{ 'color': 'yellowgreen' }}>Orlando Keen</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div id="section-5" className="uk-section uk-section-muted">
                    <div className="uk-container">
                        <div className="uk-h1 uk-text-bold uk-text-center">How it Works?</div>
                        <div className="uk-child-width-1-3@m uk-grid uk-grid-match" data-uk-grid>
                            <div>
                                <div className="uk-card uk-card-default uk-card-body uk-height-large uk-padding-remove">
                                    <div className="uk-background-muted" style={{ 'height': '15em' }}></div>
                                    <div className="uk-padding">
                                        <div className="uk-h4">DOCOOLER CHARGE CONTROLLER</div>
                                        <button className="uk-button uk-button-default">Button</button>
                                    </div>
                                </div>
                            </div>
                            <div>
                                <div className="uk-card uk-card-default uk-card-body uk-height-large uk-padding-remove">
                                    <div className="uk-background-muted" style={{ 'height': '15em' }}></div>
                                    <div className="uk-padding">
                                        <div className="uk-h4">ECEEN SOLAR PANEL CHARGER</div>
                                        <button className="uk-button uk-button-default">Button</button>
                                    </div>
                                </div>
                            </div>
                            <div>
                                <div className="uk-card uk-card-default uk-card-body uk-height-large uk-padding-remove">
                                    <div className="uk-background-muted" style={{ 'height': '15em' }}></div>
                                    <div className="uk-padding">
                                        <div className="uk-h4">ECO-WORTHY 100 WATTS DIY</div>
                                        <button className="uk-button uk-button-default">Button</button>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

                <div id="section-6" className="uk-section uk-section-muted">
                    <div className="uk-container">
                        <div className="uk-h1 uk-text-bold uk-text-center">More Useful Info</div>
                        <div className="uk-child-width-1-2@m uk-grid uk-grid-match" data-uk-grid>
                            <div>
                                <div className="uk-tile uk-tile-default uk-height-medium uk-padding-remove">
                                    <div className="uk-background-muted">

                                    </div>
                                    <div className="uk-padding">
                                        <div className="uk-h4 uk-text-uppercase">Minim nisi adipisicing anim sint ut sit.</div>
                                        <div className="uk-h5">Culpa amet est duis in veniam in. Sunt cupidatat aliqua veniam elit exercitation laboris aliqua fugiat do magna culpa. Commodo aliqua consequat deserunt nisi ullamco excepteur culpa enim elit. Labore eu laborum et id voluptate. Labore qui culpa consequat nostrud enim tempor aliquip excepteur dolor est commodo velit.</div>
                                    </div>
                                </div>
                            </div>
                            <div>
                                <div className="uk-tile uk-tile-default uk-height-medium uk-padding-remove">
                                    <div className="uk-background-muted">

                                    </div>
                                    <div className="uk-padding">
                                        <div className="uk-h4 uk-text-uppercase">Excepteur irure fugiat labore.</div>
                                        <div className="uk-h5">Culpa amet est duis in veniam in. Sunt cupidatat aliqua veniam elit exercitation laboris aliqua fugiat do magna culpa. Commodo aliqua consequat deserunt nisi ullamco excepteur culpa enim elit. Labore eu laborum et id voluptate. Labore qui culpa consequat nostrud enim tempor aliquip excepteur dolor est commodo velit.</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div id="footer" className="uk-section uk-section-primary">
                    <div className="uk-container">
                        <div className="uk-child-width-1-4@m uk-grid uk-grid-match" data-uk-grid>
                            <div>
                                <div className="uk-h5">Dolor in dolore deserunt officia veniam eu do ad exercitation id. Labore ea ad enim duis cupidatat consequat pariatur sint enim excepteur irure. Mollit eu mollit nulla ut nisi et sit proident officia reprehenderit et qui qui excepteur. Eu in eiusmod esse do qui irure ad aliqua.</div>
                                <div className="uk-h6">Powered by FinTek Solutions</div>
                            </div>
                            <div>
                            </div>
                            <div>
                            </div>
                            <div>
                                <div className="uk-h4 uk-margin-small">SIGN UP TODAY!</div>
                                <div className="uk-inline">
                                    <a href="" className="uk-icon-button uk-margin-small-right" style={{ 'color': 'white' }} data-uk-icon="icon: twitter"></a>
                                    <a href="" className="uk-icon-button  uk-margin-small-right" style={{ 'color': 'white' }} data-uk-icon="icon: facebook"></a>
                                    <a href="" className="uk-icon-button" style={{ 'color': 'white' }} data-uk-icon="icon: google-plus"></a>
                                </div>
                                <div className="uk-h5 uk-margin-small">Pariatur incididunt exercitation deserunt excepteur cillum aliquip non sit.</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div >
        );
    }
};

export default Home;