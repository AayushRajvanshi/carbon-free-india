import React, { Component } from 'react';
import Login from './Login.jsx';

class NavBar extends Component {
    render() {
        return (
            <div id="navbar" style={{'z-index': '980' }} data-uk-sticky="cls-active: uk-box-shadow-medium">
                <nav className="uk-container" data-uk-navbar>
                    <div className="uk-navbar-center">
                        <div id="nav-list">
                            <ul className="uk-navbar-nav">
                                <li><a href="#">HOME</a></li>
                                <li><a href="#">PRODUCTS</a></li>
                                <li><a href="#">ABOUT US</a></li>
                                <li><a href="#">BLOG</a></li>
                                <li><a href="#">SALE</a></li>
                                <li><a href="#">CONTACT US</a></li>
                            </ul>
                        </div>
                    </div>
                </nav>
            </div>
        );
    };
}

export default NavBar;