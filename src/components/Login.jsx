import React, { Component } from 'react';

class Login extends Component {
    constructor(props) {
        super(props);
    }

    onSignIn(googleUser) {
        console.log('user signed in'); // plus any other logic here
        console.log(googleUser); // plus any other logic here
    }

    renderGoogleLoginButton() {
        console.log('rendering google signin button');
        gapi.signin2.render('my-signin2', {
            'scope': 'https://www.googleapis.com/auth/plus.login',
            'width': 200,
            'height': 50,
            'longtitle': true,
            'theme': 'dark',
            'onsuccess': this.onSignIn
        });
    }

    componentDidMount() {
        window.addEventListener('google-loaded', this.renderGoogleLoginButton);
    }

    render() {
        return (
            <div>
                <div id="my-signin2"></div>
            </div>
        );
    }
}

export default Login;